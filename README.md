# Financial Product Subscripton

## Requirements

Following tools must be install to setup this project:

```
$ python >= 3.8

$ poetry >= 1.1

$ Docker Desktop >= 3.0
```

## Setup environment

Following command lines could be used to setup the project:
_(Please, make sure that you have the ports 8000 and 8501 opened)_

```
- by SSH
$ git clone git@gitlab.com:yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-6.git
- by HTTPS
https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-6.git

$ cd chaos-6/
$ docker-compose build  # Build API and Streamlit images
$ docker-compuse up -d # Run images
$ export GOOGLE_APPLICATION_CREDENTIALS="<path>/yotta-square-ml3-25cdb1f9af42.json" # you can find the json in CI/CD variables of Gitlab 
```

## Access to Streamlit App

About the needed csv file, you can use **mkpred.csv** located in **data/**

In order to access to the streamlit app, go to [localhost:8501](http://localhost:8501)

## Test the API

Thanks to Swagger, you can easily test the API by creating requests directly on the documentation page.
In order to test the API, you can go to 
- [localhost:8000](http://localhost:8000/docs) 
- cluster IP [35.187.78.66](http://35.187.78.66:8000/docs)
Please use the deticated csv **mkpred.csv** you can find in data folder
