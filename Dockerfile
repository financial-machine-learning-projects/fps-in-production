FROM python:3.8-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
RUN pip install poetry 

RUN poetry config virtualenvs.in-project true
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-root --no-dev

COPY . .
RUN poetry install --no-dev

EXPOSE 8000
CMD ["poetry", "run", "uvicorn", "chaos.application.api_main:app","--host" ,"0.0.0.0" , "--port" , "8000"]
