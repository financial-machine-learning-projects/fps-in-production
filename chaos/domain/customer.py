import os
import pickle
import chaos.settings.base as stg
from product_sub.domain.predicter import Predicter


class Customer:
    """Class to get the prediction about telemarketing campaign

    Returns
    -------
    np.Array
        numpy array with predictions
    """

    MODEL_PATH = os.path.join(stg.PROCESSED_DATA_DIR, "model_rfc.pkl")
    PIPE_PROCESSOR_PATH = os.path.join(stg.PROCESSED_DATA_DIR, "pipe.pkl")

    def __init__(self, filename_marketing, filename_ecosocio):
        """
        Parameters
        ----------
        marketing: filename
            marketing data, used as features for prediction. At the moment, only the following keys are used: 'AGE', 'BALANCE'
        """
        self.filename_ecosocio = filename_ecosocio
        self.filename_marketing = filename_marketing

    def predict_subscription_from_file(self):
        """Get the predictions from a file with marketing datas

        Returns
        -------
        np.Array
            Array with predictions
        """
        predicter = Predicter(
            stg.FILENAME_BANK,
            stg.FILENAME_SOCIO_ECO,
            self.PIPE_PROCESSOR_PATH,
            self.MODEL_PATH,
        )
        preds = predicter.predict(self.filename_marketing, self.filename_ecosocio)
        return preds
