"""Basic settings of the project.
Contains all configurations for the projectself.
Should NOT contain any secrets.
>>> import settings
>>> settings.DATA_DIR
"""
import os

# By default the data is stored in this repository's "data/" folder.
# You can change it in your own settings file.
REPO_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../"))

DATA_DIR = os.path.join(REPO_DIR, "data")
RAW_DATA_DIR = os.path.join(DATA_DIR, "raw")
PROCESSED_DATA_DIR = os.path.join(DATA_DIR, "processed")
INTERIM_DIR = os.path.join(DATA_DIR, "interim")
CHAOS_DIR = os.path.join(REPO_DIR, "chaos")
INFRA_DIR = os.path.join(CHAOS_DIR, "infrastructure")
CONFIG_DIR = os.path.join(INFRA_DIR, "config")
CSV_PRED_PATH = os.path.join(RAW_DATA_DIR, "data_pred.csv")
CSV_ECO_PATH = os.path.join(RAW_DATA_DIR, "socio_eco.csv")

CSV_MKPRED_PATH = os.path.join(RAW_DATA_DIR, "mkpred.csv")
CSV_ECOPRED_PATH = os.path.join(RAW_DATA_DIR, "ecopred.csv")

COLUMNS = [
    "DATE"
    "AGE"
    "JOB_TYPE"
    "STATUS"
    "EDUCATION"
    "HAS_DEFAULT"
    "BALANCE"
    "HAS_HOUSING_LOAN"
    "HAS_PERSO_LOAN"
    "CONTACT"
    "DURATION_CONTACT"
    "NB_CONTACT"
    "NB_DAY_LAST_CONTACT"
    "NB_CONTACT_LAST_CAMPAIGN"
    "RESULT_LAST_CAMPAIGN"
]

SEP = ";"

FILENAME_BANK = "data.csv"
FILENAME_SOCIO_ECO = "socio_eco.csv"